#include <xpp/display.hpp>

#include <X11/Xlib.h>

#include <stack>

namespace lukas::xpp
{
  Window Display::rootWindow()
  {
    return { *this, DefaultRootWindow(this->m_display) };
  }

  std::optional<Window> Display::findWindowByTitle(std::string_view title)
  {
    std::stack<Window> history{};
    history.push(this->rootWindow());

    while (!history.empty())
    {
      auto window{ history.top() };
      history.pop();

      if (window.title() == title)
      {
        return window;
      }
      else
      {
        for (auto& child : window.children())
        {
          history.push(child);
        }
      }
    }

    return {};
  }

  ::Display* Display::cobj()
  {
    return this->m_display;
  }

  Display::Display()
      : m_display{ XOpenDisplay(nullptr) }
  {
  }

  Display::~Display()
  {
    XCloseDisplay(this->m_display);
  }
}