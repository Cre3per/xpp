#include <xpp/window.hpp>

#include <xpp/display.hpp>
#include <xpp/exception/c_error.hpp>

#include <X11/Xlib.h>

#include <algorithm>
#include <iterator>

namespace lukas::xpp
{
  std::vector<Window> Window::children() const
  {
    unsigned int childCount{};
    ::Window* children{};

    if (XQueryTree(this->m_display.get().cobj(), this->m_window, nullptr, nullptr, &children, &childCount) == 0)
    {
      throw CError{};
    }
    else
    {
      std::vector<Window> result{};
      result.reserve(childCount);
      std::transform(children, children + childCount, std::back_inserter(result), [this](auto child) {
        return Window{ this->m_display.get(), child };
      });
      XFree(children);
      return result;
    }
  }

  std::string Window::title() const
  {
    char* name{};
    if (XFetchName(this->m_display.get().cobj(), this->m_window, &name) == 0)
    {
      std::string result{ name };
      XFree(name);
      return result;
    }
    else
    {
      return {};
    }
  }

  Rectangle Window::bounds() const
  {
    XWindowAttributes attributes{};

    XGetWindowAttributes(this->m_display.get().cobj(), this->m_window, &attributes);

    return { attributes.x, attributes.y, attributes.width, attributes.height };
  }

  Window::Window(Display& display, ::Window window)
      : m_display{ display },
        m_window{ window }
  {
  }
}