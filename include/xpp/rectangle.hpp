#if !defined(HPP_xpp_rectangle)
#define HPP_xpp_rectangle

namespace lukas::xpp
{
  struct Rectangle
  {
    int x{};
    int y{};
    int width{};
    int height{};
  };
}

#endif