#if !defined(HPPxpp_c_error)
#define HPPxpp_c_error

#include <cstring>
#include <errno.h>
#include <stdexcept>

namespace lukas::xpp
{
  class CError : public virtual std::runtime_error
  {
  public:
    CError()
        : std::runtime_error{ std::strerror(errno) }
    {
    }
  };
}

#endif