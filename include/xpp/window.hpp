#if !defined(HPP_xpp_window)
#define HPP_xpp_window

#include <xpp/rectangle.hpp>

#include <X11/X.h>

#include <functional>
#include <string>
#include <vector>

namespace lukas::xpp
{
  class Display;

  /**
   * Handle to a window.
   */
  class Window
  {
  private:
    std::reference_wrapper<Display> m_display;
    ::Window m_window{};

  public:
    /**
     * @return A list of direct children.
     */
    [[nodiscard]] std::vector<Window> children() const;

    /**
     * @return The window's title.
     */
    [[nodiscard]] std::string title() const;

    /**
     * @return Position and dimensions.
     */
    [[nodiscard]] Rectangle bounds() const;

  public:
    /**
     * @param window X11 handle to the window.
     */
    Window(Display& display, ::Window window);

  public:
    /**
     * @return An invalid window.
     */
    [[nodiscard]] static Window invalid() noexcept;
  };
}

#endif