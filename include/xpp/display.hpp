#if !defined(HPPxpp_display)
#define HPPxpp_display

#include <xpp/window.hpp>

#include <X11/Xlib.h>

#include <optional>
#include <string_view>

namespace lukas::xpp
{
  class Display
  {
  private:
    ::Display* m_display{};

  public:
    /**
     * @return The root window for this display.
     */
    [[nodiscard]] Window rootWindow();

    /**
     * Searches for a window by its title.
     * @param title The window's title.
     */
    [[nodiscard]] std::optional<Window> findWindowByTitle(std::string_view title);

    [[nodiscard]] ::Display* cobj();

  public:
    Display& operator=(const Display&) = delete;

  public:
    /**
     * Obtains the display from the DISPLAY environment variable.
     */
    Display();

    Display(const Display&) = delete;

    ~Display();
  };
}

#endif