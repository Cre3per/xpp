from conans import ConanFile, Meson, tools

class XppConan(ConanFile):
  name = "xpp"
  version = "0.1"
  settings = "os", "compiler", "build_type", "arch"
  generators = "pkg_config"
  exports_sources = "*"
  no_copy_source = True

  def build(self):
    meson = Meson(self)
    meson.configure()
    meson.build()

  def package(self):
    self.copy("*.hpp")
    self.copy("*.dll", dst="bin", keep_path=False)
    self.copy("*.so", dst="lib", keep_path=False)
    self.copy("*.dylib", dst="lib", keep_path=False)
    self.copy("*.a", dst="lib", keep_path=False)

  def package_info(self):
    self.cpp_info.libs = ["xpp"]