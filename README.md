# xpp

C++ wrappers for some parts of the X11 library.

## Building

```
mkdir ./build/
cd ./build/
conan install ..
conan build ..
```

Subsequent builds

```
ninja
```